<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
	<?php do_action('footerJScrollPane'); ?>
	<script>
		(function ($) {
			$('#primary').on('click', '.small-nav-item', function () {
				var c1 = $(this).data('toggle'),
					c2 = $(this).data('class');
				/* show class */
				$('#content').find('.'+c1)
					.hide()
					.filter('.'+c2)
					.show();
				/* toggle */
				$(this).addClass('active')
					.siblings()
					.removeClass('active');
			});
		})(jQuery);
	</script>
</body>
</html>