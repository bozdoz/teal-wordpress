<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<article>	
		<span>
		Select by type: 
		</span>
		<ul class="small-nav">
			<li class="small-nav-item active" data-toggle="project" data-class="all">All</li>
			<li class="small-nav-item" data-toggle="project" data-class="room">Room</li>
			<li class="small-nav-item" data-toggle="project" data-class="building">Building</li>
			<li class="small-nav-item" data-toggle="project" data-class="city">City</li>
		</ul>

		<div id="content" class="site-content projects" role="main">

		<?php 
		
		$args = array(
			'posts_per_page'   => 25,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'project',
			'post_status'      => 'publish'); 
		
		$projects = get_posts( $args );
		foreach ( $projects as $project ) {
			$term_list = wp_get_post_terms($project->ID, 'project_category', array("fields" => "names"));
			?> 
		<a class="project all<?php
			foreach ($term_list as $term) {
				echo ' ' . str_replace(' ', '', strtolower($term)); 
			} 
			?>" href="<?php echo get_permalink($project->ID); ?>">
		<?php 
		?>
			<?php echo get_the_post_thumbnail($project->ID, 'project-thumb'); ?>
			<span><?php echo $project->post_title; ?></span>
		</a>
		<?php
		}
		wp_reset_postdata();
			?>
	
		</div><!-- #content -->
		</article> <!-- #post -->
	</div><!-- #primary work -->

<?php get_footer('work'); ?>