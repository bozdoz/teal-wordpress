<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
	
		<div id="content" class="site-content employees" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
				
					<?php if ( has_post_thumbnail() ) : ?>
					<div class="entry-thumbnail">
						<?php the_post_thumbnail('full'); ?>
					</div>
					<?php endif; ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php /* comments_template(); */ ?>
			<?php endwhile; ?>
			
		<?php 
		
		$args = array(
			'posts_per_page'   => 25,
			'orderby'          => 'id',
			'order'            => 'ASC',
			'post_type'        => 'team',
			'post_status'      => 'publish'); 
		
		$teams = get_posts( $args );
		foreach ( $teams as $team ) {
			?> 
		<article class="team clearfix">
			<div class="entry-content">
				<div class="entry-thumbnail" style="float:left; margin-right:15px;">
					<a href="<?php echo post_permalink($team->ID); ?>">
				<?php echo get_the_post_thumbnail($team->ID, 'thumbnail'); ?>
				</a>
				</div>
				<h2 class="entry-title"><a href="<?php echo post_permalink($team->ID); ?>"><?php echo $team->post_title; ?></a></h2>
				<?php echo $team->post_excerpt; ?>
			</div>
		</article>
		<?php
		}
		wp_reset_postdata();
			?>
	
		</div><!-- #content -->
	</div><!-- #primary work -->

<?php get_footer('work'); ?>