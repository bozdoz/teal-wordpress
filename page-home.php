<?php
/*
 * Template Name: Home
 * Description: Story book cover page
 */

get_header('home'); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			
					<div class="image-slide-show">
						<?php the_content(); ?>
						<div class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-meta -->
					</div><!-- .entry-content -->
			<?php endwhile; ?>
		</div><!-- #content -->
	</div><!-- #primary -->
	
<?php get_footer('home'); ?>