<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<ul class="small-nav">
			<li class="small-nav-item"> <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'About' ) ) ); ?>">&larr; The Team</a></li>
		</ul>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail('full'); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->
					<div class="entry-content">
					<table class="employee-details details">
						<?php
							$meta = get_post_meta(get_the_ID());
							foreach($meta as $k=>$v) {
								if (strpos($k, 'team_') === 0 && $v[0]) {
						?>
						<tr>
						<?php if ($k == 'team_twittername') { ?>
						<th>Twitter</th>
						<td><?php echo do_shortcode('[social twitter="'.$v[0].'"]'); ?></td>
						<?php
						} else {
						?>
						<th><?php echo ucwords(str_replace('team_', '', $k)); ?></th>
						<td><?php echo $v[0]; ?></td>
						<?php } ?>
						</tr>
						<?php
								}
							}
						?>
					</table>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->
				<?php /* comments_template(); */ ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary employee -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>