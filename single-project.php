<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="align-right">
		<?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
		</div>
		<ul class="small-nav">
			<li class="small-nav-item"> <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Work' ) ) ); ?>">&larr; Projects</a></li>
		</ul>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail('full'); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->
					<div class="entry-content">
					<table class="project-details details">
						<?php
							$meta = get_post_meta(get_the_ID());
							foreach($meta as $k=>$v) {
								if (strpos($k, 'project_') === 0 && $v[0]) {
						?>
						<tr>
						<th><?php echo ucwords(str_replace('project_', '', $k)); ?></th>
						<td><?php echo $v[0]; ?></td>
						</tr>
						<?php
								}
							}
						?>
					</table>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->
				<?php /* comments_template(); */ ?>

			<?php endwhile; ?>

			<script>
			(function ($) {
				/* links go elsewhere */
				$('a').has('img').attr('target', '_blank');
			})(jQuery);
			</script>
		</div><!-- #content -->
	</div><!-- #primary project -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>